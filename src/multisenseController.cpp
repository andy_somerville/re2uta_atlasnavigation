/**
 *
 * multisenseController: This will allows setting the speed and other settings of the Atlas laser
 *
 * rosrun uta_drc_robotSense multisenseController
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see http://gazebosim.org/wiki/Tutorials/drcsim/2.2/multisense_sl_data
 * @created 03/14/2013
 * @modified 03/14/2013
 *
 */

#include <ros/ros.h>
#include <std_msgs/Float64.h>

/*class multisenseController
{
 public:
          multisenseController()
          {



            // TODO Parameterize this and maybe provide topics/services to change it




          }

 private:



};*/

int main(int argc, char** argv){

  ros::init(argc, argv, "atlas_multisenseController");

  ros::NodeHandle node;
  // create ROS topics
  ros::Publisher spindle_speed_sub_ = node.advertise<std_msgs::Float64>("/multisense_sl/set_spindle_speed", 1, true);

  std_msgs::Float64 multisenseCmd;
  multisenseCmd.data = 1.0;

  spindle_speed_sub_.publish(multisenseCmd);

  ros::spin();

}
